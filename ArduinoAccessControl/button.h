#include "Arduino.h"
//////////////////
//  HEADER////////
//////////////////
class Button{
  public:
    Button(int pin, int trigger_count);
    bool isButtonPressed();
  private:
    int _pinNumber;
    unsigned int _pressCount;
    unsigned int _trigger;
    void _incrementPressCountOrClear();
};



//////////////////////
/// IMPLEMENTATION ///
//////////////////////
Button::Button(int pin, int trigger_count)
{
  _pinNumber = pin;
  _pressCount = 0;
  _trigger = trigger_count;
  pinMode(pin, INPUT);
}

bool Button::isButtonPressed()
{
  _incrementPressCountOrClear();
  if (_pressCount>_trigger)
  {
    _pressCount=0;
    return true;
  }
  else
  {
    return false;
  }
}


void Button::_incrementPressCountOrClear()
{
  if (digitalRead(_pinNumber))
  {
    _pressCount++;
  }
  else
  {
    _pressCount=0; 
  }
}





