#include <Wire.h>
#include <EEPROM.h>
#include "NFC_I2C.h"
#include "button.h"
#include "keyMemory.h"

#define IRQ   (2)
#define RESET (3)
#define SELECT_SWITCH_1 (4)
#define SELECT_SWITCH_2 (5)
#define SELECT_SWITCH_3 (6)
#define SELECT_SWITCH_4 (7)
#define REDLED (8)
#define GREENLED (9)
#define BUZZER (10)
#define RESET_BUTTON (11)
#define DOOR_RELAY (12)
#define ACCESS_BUTTON (13)

#define SELECT_SWITCH_TIME_PER_BIT (100)
uint8_t master_key[] = { 0xCB, 0x88, 0xD4, 0xEC }; // ID of master key

NFC_I2C nfc(IRQ, RESET);
Button reset_button(RESET_BUTTON, 60000); // 60k loops ~2sec
Button access_button(ACCESS_BUTTON, 3000);
KeyMemory key_storage;


void setup(void) {
  
  pinMode(SELECT_SWITCH_1, INPUT);
  pinMode(SELECT_SWITCH_2, INPUT);
  pinMode(SELECT_SWITCH_3, INPUT);
  pinMode(SELECT_SWITCH_4, INPUT);
  pinMode(REDLED,OUTPUT);
  pinMode(GREENLED,OUTPUT);
  pinMode(BUZZER,OUTPUT);
  pinMode(DOOR_RELAY, OUTPUT);
  
  Serial.begin(115200);
  nfc.begin();
  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    digitalWrite(REDLED,HIGH);
    while (1); // halt
  }
  
  nfc.setPassiveActivationRetries(0xFF);
  nfc.SAMConfig();
  nfc.setCardToReadMode(PN532_MIFARE_ISO14443A);
}


void loop(void) {

  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
  
  // Reset EEPROM by holding button
  if(reset_button.isButtonPressed()){
     beep(750);
     key_storage.dumpEeprom();
     key_storage.reset();
  }
  
  if(access_button.isButtonPressed()){
     activate_door(get_door_activation_time());
     delay(250);
  }

  if ( nfc.isCardDataAvailable() ){
    beep(100);
    nfc.readCardData(&uid[0], &uidLength);
    print_card_info(uid, uidLength);
    
    // If the key is the master key then we need to go into programming mode
    if (compare_keys(uid, master_key)){add_key_mode();}
    else if(key_storage.isKeyPresent(uid)){grant_access();}
    else{deny_access();}
    
    // make sure to wait a few secs before resetting
    nfc.setCardToReadMode(PN532_MIFARE_ISO14443A);
  }

}



////////////////////////////////////////////
////////////////////////////////////////////
/////////                       ////////////
/////////    HELPER FUNCTIONS   ////////////
/////////                       ////////////
////////////////////////////////////////////
////////////////////////////////////////////


int get_door_activation_time(void){
  int total_time = 0;
  total_time +=  digitalRead(SELECT_SWITCH_1) * SELECT_SWITCH_TIME_PER_BIT;
  delay(20);
  total_time +=  digitalRead(SELECT_SWITCH_2) * SELECT_SWITCH_TIME_PER_BIT * 2;
  delay(20);
  total_time +=  digitalRead(SELECT_SWITCH_3) * SELECT_SWITCH_TIME_PER_BIT * 4;
  delay(20);
  total_time +=  digitalRead(SELECT_SWITCH_4) * SELECT_SWITCH_TIME_PER_BIT * 8;
  return total_time;
}


void beep(int beep_time){
  digitalWrite(BUZZER,HIGH);
  delay(beep_time);
  digitalWrite(BUZZER,LOW);
}


void multi_beep(int beep_time, int count, int delay_time){
  for(int i=0; i<count;i++){
    beep(beep_time);
    //add delay on all except last loop
    if (i+1!=count){delay(delay_time);}
  }
}


void activate_door(int open_time){
  digitalWrite(DOOR_RELAY,HIGH);
  delay(open_time);
  digitalWrite(DOOR_RELAY,LOW);
}


void grant_access(void){
  digitalWrite(GREENLED,HIGH);
  int door_activation_time = get_door_activation_time();
  activate_door(door_activation_time);
  if (door_activation_time < 1500){delay(1500-door_activation_time);}
  digitalWrite(GREENLED,LOW);

}


void deny_access(void){
  digitalWrite(REDLED,HIGH);
  delay(1500);
  digitalWrite(REDLED,LOW);
}


// returns true if the keys (first 4 items in both arrays) match
boolean compare_keys(uint8_t key1[], uint8_t key2[]){
  for (int i=0; i<4; i++ ){
    if (key1[i] != key2[i]){
      return 0;
    }
  }
  return 1;
}


// print card details returned from the NFC library - put here to clean up main loop
void print_card_info(uint8_t uid[], uint8_t uidLength){
  
  Serial.println("Found a card!");
  Serial.print("UID Length: ");Serial.print(uidLength, DEC);Serial.println(" bytes");
  Serial.print("UID Value: ");
  for (uint8_t i=0; i < uidLength; i++){Serial.print(" 0x");Serial.print(uid[i], HEX);}
  Serial.println("");
}


// user wants to add a key to the EEPROM.  This prompts the user to do so and waits ~16 seconds to allow user to do so.
void add_key_mode(){
  digitalWrite(REDLED,HIGH);
  delay(750);
  multi_beep(50,3,50);
  delay(750);
  nfc.setCardToReadMode(PN532_MIFARE_ISO14443A);

  for(int i = 0; i<15; i++){
    digitalWrite(REDLED,HIGH);
    if ( nfc.isCardDataAvailable() ){
      beep(100);
      add_key_to_eeprom();
      i=1000;
    }
    delay(600);
    digitalWrite(REDLED,LOW);
    delay(600);
  }
  
}


// add card to the EEPROM if not already there.
void add_key_to_eeprom(){
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
  nfc.readCardData(&uid[0], &uidLength);
  print_card_info(uid, uidLength);
  if(key_storage.isKeyPresent(uid) || compare_keys(uid, master_key)){
    //key already here
    Serial.println("Card already present");
  }
  else{
    Serial.println("adding new card");
    key_storage.addKey(uid);
    delay(500);
    beep(750);
  }
  nfc.setCardToReadMode(PN532_MIFARE_ISO14443A);
}

